package com.duobab.springbootangularbackdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAngularBackDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAngularBackDemoApplication.class, args);
    }

}
