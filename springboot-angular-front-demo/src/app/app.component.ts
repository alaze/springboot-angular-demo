import {Component} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'springboot-angular-front-demo';
  website;

  constructor(private http: HttpClient) {

  }

  request() {
    const headers = new HttpHeaders().set('Content-type', 'application/json; charset=UTF-8');
    this.http.get('http://localhost:81/website')
      .subscribe((res: any) => {
        if (res) {
          this.website = res.url;
        }
      }, error => {
        console.log(error);
      });

  }
}
