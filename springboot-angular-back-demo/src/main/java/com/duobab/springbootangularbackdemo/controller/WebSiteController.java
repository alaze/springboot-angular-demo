package com.duobab.springbootangularbackdemo.controller;

import com.duobab.springbootangularbackdemo.domain.WebSite;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/website")
@Log
public class WebSiteController {

    @RequestMapping(method = RequestMethod.GET, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public WebSite getWebSite() {
        return WebSite.ofWebSite("http://www.duobab.com");
    }
}
