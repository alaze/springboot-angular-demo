<img src="https://gitee.com/duobab/duobab/raw/master/res/images/duo.jpg" height="100" width="200">

[![license](https://img.shields.io/github/license/seata/seata.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

[duobab.com](http://www.duobab.com) 

## 简介

基于spring boot + angular 的前后端分离工程demo
- 拉取代码
- 运行SpringbootAngularBackDemoApplication类中的main方法启动后端工程
- 进入springboot-angular-front-demo所在目录, 执行 `ng serve --port 80`启动前端工程
- 本地浏览器访问 http://localhost

