package com.duobab.springbootangularbackdemo.domain;

import lombok.Data;

@Data
public class WebSite {
    private String url;

    private WebSite(String url) {
        this.url = url;
    }

    public static WebSite ofWebSite(String url) {
        return new WebSite(url);
    }
}
